FROM debian:jessie
MAINTAINER Jack Henschel <jh@openmailbox.org>

ENV LE='/opt/letsencrypt.sh'

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    curl openssl grep git python3-pip

RUN ln -s /usr/bin/python3 /usr/local/bin/python

RUN git clone 'https://github.com/lukas2511/letsencrypt.sh' "$LE" \
    && mkdir -p "${LE}/hooks" \
    && git clone 'https://github.com/kappataumu/letsencrypt-cloudflare-hook' "${LE}/hooks/cloudflare" \
    && pip3 install -r "${LE}/hooks/cloudflare/requirements.txt"

ADD config.sh "${LE}"
ADD domains.txt "${LE}"

VOLUME ["${LE}/certs", "${LE}/accounts"]

CMD "${LE}/letsencrypt.sh" --cron --config "${LE}/config.sh"
