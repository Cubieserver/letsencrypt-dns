################################################################################
# This is the main config file for letsencrypt.sh                              #
# https://github.com/lukas2511/letsencrypt.sh/blob/master/docs/examples/config #
################################################################################

# LE env variable is exported in Dockerfile
BASEDIR=$LE

CA="https://acme-v01.api.letsencrypt.org/directory"
DOMAINS_TXT="${BASEDIR}/domains.txt"
CONTACT_EMAIL='someone@example.com'

KEYSIZE="4096"
KEY_ALGO=rsa

# Regenerate private keys instead of just signing new certificates on renewal (default: yes)
#PRIVATE_KEY_RENEW="yes"
# Option to add CSR-flag indicating OCSP stapling to be mandatory (default: no)
#OCSP_MUST_STAPLE="no"

# Cloudflare DNS Hook
CHALLENGETYPE="dns-01"
HOOK=${BASEDIR}/hooks/cloudflare/hook.py
export CF_EMAIL='someone@example.com'
export CF_KEY='YOUR_CLOUDFLARE_TOKEN'

